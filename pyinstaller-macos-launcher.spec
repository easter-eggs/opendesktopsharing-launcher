# -*- mode: python -*-

block_cipher = None


a = Analysis(['opendesktopsharing_launcher/__init__.py'],
             pathex=[],
             binaries=[
             ],
             datas=[
                 ('opendesktopsharing_launcher/share', './share',),
             ],
             hiddenimports=[
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[
             ],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='OpenDesktopSharing-Launcher-macos',
          debug=False,
          strip=True,
          upx=False,
          console=False, )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=True,
               upx=False,
               console=False,
               name='OpenDesktopSharing-Launcher-macos')
app = BUNDLE(coll,
             console=False,
             name='OpenDesktopSharing-Launcher-macos.app',
             icon='share/images/odc.icns',
             bundle_identifier=None)


