
#! /usr/bin/env python
# -*- coding: utf-8 -*-

from builtins import isinstance
from sys import argv
import threading
from os import system
from optparse import OptionParser
import appdirs
import hashlib
import logging
import ntpath
import platform
import requests
import shutil
import stat
import struct
import subprocess
import sys
import tarfile
import time
import os
import tempfile
from PIL import ImageTk, Image

import pkg_resources
import pkgutil

from tkinter import Tk, TOP, BOTTOM, LEFT, RIGHT, BOTH, RAISED, scrolledtext, END, Canvas, PhotoImage, Label
from tkinter.ttk import Frame, Button, Style
try:
    import distro
except:
    distro = None


__version__ = 0.4



def search_share_path(path):
    path = '{}'.format(os.sep).join(path.split('/'))
    bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
    if os.path.exists(path):
        pass
    else:
        if os.path.exists(os.path.join(bundle_dir, path)):
            path = os.path.join(bundle_dir, path)
        else:
            if os.path.exists(os.path.join('opendesktopsharing_launcher', path)):
                path = os.path.join('opendesktopsharing_launcher',  path)
            else:
                if os.path.exists(os.path.join(bundle_dir, 'opendesktopsharing_launcher', path)):
                    path = os.path.join(bundle_dir, 'opendesktopsharing_launcher',  path)
                else:
                    if os.path.exists(os.path.join(bundle_dir, path)):
                        path = os.path.abspath(os.path.join(bundle_dir, path))
                    else:
                        if os.path.exists(os.path.join(bundle_dir, 'opendesktopsharing_launcher', path)):
                            path = os.path.join(bundle_dir, 'opendesktopsharing_launcher', path)
    return path

APP_NAME = "OpenDesktopSharing - Launcher"
THEME_PATH = search_share_path('share/styles/Azure-ttk-theme/azure.tcl')
LOGO_PATH = search_share_path('share/images/logo-medium.png')
ICONE_PATH = search_share_path('share/images/icon.png')
DOWNLOAD_URL = 'https://opendesktopsharing.easter-eggs.com/download/{}/ods/{}/latest/{}'


log = logging.getLogger(APP_NAME)

def logging_setup(options):

    log = logging.getLogger(APP_NAME)
    if options.debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    # Console handler (root logger)
    handler = logging.StreamHandler(sys.stderr)
    if options.debug:
        handler.setLevel(logging.DEBUG)
    elif options.info:
        handler.setLevel(logging.INFO)
    else:
        handler.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)-5.5s %(message)s')
    handler.setFormatter(formatter)

    log.addHandler(handler)

def humanbytes(B):
    """
    Return the guestn bytes as a human friendly KB, MB, GB, or TB string
    From: https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
    """
    B = float(B)
    KB = float(1024)
    MB = float(KB ** 2) # 1,048,576
    GB = float(KB ** 3) # 1,073,741,824
    TB = float(KB ** 4) # 1,099,511,627,776

    if B < KB:
        return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
        return '{0:.2f} KB'.format(B/KB)
    elif MB <= B < GB:
        return '{0:.2f} MB'.format(B/MB)
    elif GB <= B < TB:
        return '{0:.2f} GB'.format(B/GB)
    elif TB <= B:
        return '{0:.2f} TB'.format(B/TB)

class launcher(Frame):

    def __init__(self, options=None):
        super().__init__()
        self.options = options
        self.initUI()

    def initUI(self):
        self.master.title("OpenDesktopSharing - launcher {}".format(__version__))

        self.style = Style()
        self.style.theme_use("default")

        try:
            self.tk.call("source", THEME_PATH)
            self.tk.call("set_theme", "light")
        except Exception as e:
            log.warning(e)

        frame = Frame(
            self,
            relief=RAISED,
            borderwidth=1
        )
        frame.pack(fill=BOTH, expand=True)

        self.pack(fill=BOTH, expand=True)
        self.grid_columnconfigure(0, weight=1)

        img = ImageTk.PhotoImage(file=LOGO_PATH)
        label = Label(self, image=img)
        label.image = img
        label.place(x=0, y=0, relwidth=1, relheight=1)
        label.pack(
            side=TOP,
            expand=True,
            fill='both',
            padx=0,
            pady=0,
        )

        self.message_box = scrolledtext.ScrolledText(
            self,
            width=64,
            height=7,
            # bg='black',
            # fg='white'
        )
        self.message_box.insert(END, 'OpenDesktopSharing launcher {}, verifing update...\n\n'.format(__version__))
        self.message_box.config(command=None)
        self.message_box.pack()

        quit_button = Button(
            self,
            text="Cancel / Close",
            command=self.quit
        )
        quit_button.pack()

        self.master.withdraw()
        t = threading.Thread(target=self.launcher)
        t.daemon = True
        t.start()

    def quit(self):
        print('\nExiting')
        self.master.destroy()

    def print(self, text, return_line=True):
        if isinstance(text, list):
            text = ' '.join(text)
        if return_line:
            text += '\n'
        self.message_box.insert(END, text)
        self.message_box.see(END)

    def check_download_result(self, local_archive, online_md5):
        log.debug('Check download')
        status = True
        st = os.stat(local_archive)
        os.chmod(local_archive, st.st_mode | stat.S_IEXEC)
        if online_md5.split()[0] != self.md5(local_archive):
            log.debug('MD5 checksum failed, download error...')
            log.debug('Online MD5:\n{}'.format(online_md5.split()[0]))
            log.debug('Local MD5:\n{}'.format(self.md5(local_archive)))
            status = False
        else:
            log.debug('OK')
        return status

    def check_need_download(self, application_dir, online_md5, local_archive, local_binary):
        log.debug('Check last version')
        need_download = True
        if os.path.exists(local_binary) and os.path.exists(local_archive):
            log.debug('Check local existing version...')
            if online_md5.split()[0] == self.md5(local_archive):
                log.debug('App is up to date, no download')
                need_download = False
            else:
                self.print('A new version exists')
                log.debug('A new version exists')
                log.debug('Updating...')
        else:
            shutil.rmtree(application_dir)
            os.makedirs(application_dir)
            log.debug('Application is not installed, download needed')
        return need_download

    def download_file(self, url=None, local_archive=None):
        self.print('Downloading/Updating latest version, please wait a minute...')
        log.debug('Downloading/Updating latest version, please wait a minute...')
        with requests.get(url, stream=True) as r:
            total_length = int(r.headers.get('content-length'))
            log.debug('Download size: {}'.format(humanbytes(total_length)))
            r.raise_for_status()
            passed_percent = []
            with open(local_archive, 'wb') as f:
                dl = 0
                for i, chunk in enumerate(r.iter_content(chunk_size=8192)):
                    dl += len(chunk)
                    percent = int(100 * dl / total_length)
                    if i%100 == 0:
                        if percent%10 == 0 and percent not in passed_percent:
                            passed_percent.append(percent)
                            self.print('{}%'.format(percent), return_line=False)
                            log.debug('{}%'.format(percent))
                        self.print('.', return_line=False)

                    f.write(chunk)
        log.debug('')
        log.debug('Done')
        return local_archive

    def get_online_md5(self, url):
        url = '{}.md5'.format(url)
        r = requests.get(url)
        log.debug('Online MD5:\n{}'.format(r.text))
        if r.status_code != 200:
            log.debug('Server error: NOT FOUND: {}'.format(url))
            self.print('Server error: NOT FOUND: {}'.format(url))
            self.master.deiconify()
            return None
        return r.text

    def md5(self, local_archive):
        hash_md5 = hashlib.md5()
        with open(local_archive, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def launcher(self):
        archive_name = 'OpenDesktopSharing_{}.tar.gz'
        system = platform.system()
        binary_name = None
        sub_path = None
        runnable = True
        log.debug('Your system:\n{}'.format(platform.platform()))
        if system == 'Linux':
            log.debug('{} {} {}'.format(distro.id(), distro.version(), distro.name()))
            binary_name = 'OpenDesktopSharing-x86_64.AppImage'
            sub_path = ''.join(filter(str.isalnum, '{}{}'.format(distro.id(), distro.version())))
            if sub_path == 'debian9':
                sub_path = 'linuxold'
            else:
                sub_path = 'linux'
            archive_name = 'OpenDesktopSharing-x86_64.AppImage'
        elif system == 'Windows':
            binary_name = 'OpenDesktopSharing{}OpenDesktopSharing.exe'.format(os.sep)
            is_64bit = struct.calcsize('P') * 8 == 64
            if is_64bit:
                sub_path = 'win64'
            else:
                sub_path = 'win32'
            archive_name = archive_name.format(sub_path)
        # elif system == 'Darwin':
        #     binary_name = 'OpenDesktopSharing.dmg'
        #     sub_path = 'macos-catalina'
        else:
            log.debug('System not supported: {}'.format(platform.platform()))
            self.print('System not supported:\n{}'.format(platform.platform()))
            self.master.deiconify()
            runnable = False
        if runnable:
            target_version = "testing" if self.options.testing else "stable"
            url = DOWNLOAD_URL.format(target_version, sub_path, archive_name)
            log.debug('Download url:\n{}'.format(url))

            application_dir = appdirs.user_data_dir() + os.sep + 'opendesktopsharing-updater'
            if not os.path.exists(application_dir):
                os.makedirs(application_dir)
            local_archive = '{}{}{}'.format(application_dir, os.sep, archive_name)
            local_binary = '{}{}{}'.format(application_dir, os.sep, binary_name)

            log.debug('Archive path:\n{}'.format(local_archive))
            online_md5 = self.get_online_md5(url)

            if online_md5:
                if self.options.force or self.check_need_download(application_dir, online_md5, local_archive, local_binary):
                    self.master.deiconify()
                    self.download_file(url=url, local_archive=local_archive)
                    if not self.check_download_result(local_archive, online_md5):
                        log.debug('Download failed, exiting')
                        self.quit()
                        return
                    if local_archive.endswith('tar.gz'):
                        tar = tarfile.open(local_archive, "r:gz")
                        tar.extractall(path=application_dir)
                        tar.close()
                log.debug('Binary path:\n{}'.format(local_binary))
                log.debug('Now starting OpendesktopSharing...')

                if system == 'Linux':
                    st = os.stat(local_binary)
                    os.chmod(local_binary, st.st_mode | stat.S_IEXEC)
                    subprocess.Popen([local_binary]).pid
                elif system == "Windows":
                    # TODO: Not tested!
                    DETACHED_PROCESS = 0x00000008
                    subprocess.Popen([local_binary], creationflags=DETACHED_PROCESS).pid
                elif system == "Darwin":
                    subprocess.Popen([local_binary], close_fds=True).pid
                log.debug('Exiting launcher')
                if self:
                    self.quit()
                    return
                else:
                    sys.exit(2)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = OptionParser('usage: %prog [options]')

    parser.add_option(
        '--version',
        action='store_true',
        help='Show version'
    )

    parser.add_option(
        '--debug',
        action='store_true',
        help='Enable debug mode'
    )

    parser.add_option(
        '--info',
        action='store_true',
        help='Enable info mode'
    )

    parser.add_option(
        '--force',
        action='store_true',
        help='Force update'
    )

    parser.add_option(
        '--testing',
        action='store_true',
        help='Force testing version'
    )

    options, arguments = parser.parse_args(argv)

    logging_setup(options)
    if options.version:
        print("""{} {}""".format(APP_NAME, __version__))
        sys.exit(0)

    root = Tk()
    root.title("{} {}".format(APP_NAME, __version__))
    img = ImageTk.PhotoImage(file=ICONE_PATH)
    root.tk.call('wm', 'iconphoto', root._w, img)
    app = launcher(options=options)
    root.mainloop()


if __name__ == '__main__':
    main(sys.argv)