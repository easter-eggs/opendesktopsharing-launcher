#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing launcher



Get OpenDesktopSharing's latest version

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

import glob
import io
import os

from setuptools import find_packages, setup
from distutils.command.build import build

here = os.path.abspath(os.path.dirname(__file__))

version = 0.1
requires = [
    "appdirs",
    "distro",
    "requests",
    "tk",
    "pyinstaller",
    "pillow",
]
data_dir = "opendesktopsharing_launcher"
data_files = [
    (
        "opendesktopsharing_launcher", [
            os.path.join("opendesktopsharing_launcher/share/images/odc.ico"),
            os.path.join("opendesktopsharing_launcher/share/images/logo-medium.png"),
            os.path.join("opendesktopsharing_launcher/share/images/icon.png"),

        ] + [el for el in glob.iglob('opendesktopsharing_launcher/share/styles/**/*', recursive=True) if os.path.isfile(el)]
    )
]
setup(
    name='opendesktopsharing_launcher',
    version=str(version),
    description="Get OpenDesktopSharing's latest version",
    classifiers=[
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP",
    ],
    author='Pierre ARNAUD',
    author_email='parnaud@easter-eggs.com',
    url='https://www.opendesktopsharing.org',
    keywords='remote desktop distant access tunnel websocket',
    data_files=data_files,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    install_requires=requires,
    python_requires='>=3.5',
    entry_points="""\
    [console_scripts]
    opendesktopsharing-launcher = opendesktopsharing_launcher:main
    """,
    message_extractors = {},
)
