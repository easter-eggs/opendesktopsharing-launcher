# -*- mode: python -*-

block_cipher = None


a = Analysis(['opendesktopsharing_launcher/__init__.py'],
             pathex=[],
             binaries=[],
             datas=[
                ('opendesktopsharing_launcher\\share', './opendesktopsharing_launcher/share',),
             ],
             hiddenimports=["pynput.keyboard._win32", "pynput.mouse._win32"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='OpenDesktopSharing-launcher.exe',
          icon='opendesktopsharing_launcher\\share\\images\\odc.ico',
          debug=False,
          strip=False,
          upx=False,
          console=False)
