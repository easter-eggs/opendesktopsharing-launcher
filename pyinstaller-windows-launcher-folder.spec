# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['opendesktopsharing_launcher\\__init__.py'],
             pathex=['Z:\\builds\\0\\project-0'],
             binaries=[],
             datas=[
                ('opendesktopsharing_launcher\\share', 'opendesktopsharing_launcher\\share',),
               ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='OpenDesktopSharing-Launcher',
          icon='opendesktopsharing_launcher\\share\\images\\odc.ico',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='OpenDesktopSharing-Launcher')
