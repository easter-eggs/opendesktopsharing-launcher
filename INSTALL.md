
# Install

## Debian 9

    $ apt install -y python3-pip python3-setuptools python3-setuptools-scm python3-pkgconfig python3-tk
    $ python3 -m pip install -U setuptools
    $ python3 -m pip install .

## Debian 10

    $ apt install -y python3-pip python3-setuptools python3-tk
    $ python3 -m pip install .

## Debian 11

    $ apt install -y python3-pip python3-setuptools python3-tk
    $ python3 -m pip install .

## Windows

    $ pip install .